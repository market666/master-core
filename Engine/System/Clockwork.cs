﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RocketCore.Engine.Replication;

namespace RocketCore
{
    class Clockwork
    {
        private Timer MM_Timer;
        private Timer SL_Timer;
        private Timer TP_Timer;
        private Timer TS_Timer;
        private Timer Fix_Timer;

        private int mm_interval;
        private int sl_interval;
        private int tp_interval;
        private int ts_interval;
        private TimeSpan fix_interval;

        internal Clockwork()
        {
            //УБРАТЬ ХАРДКОД В ЭТОМ КОНСТРУКТОРЕ
            mm_interval = 1000;
            MM_Timer = new Timer(MM_Tick, null, mm_interval, Timeout.Infinite);

            sl_interval = 1000;
            SL_Timer = new Timer(SL_Tick, null, sl_interval, Timeout.Infinite);

            tp_interval = 1000;
            TP_Timer = new Timer(TP_Tick, null, tp_interval, Timeout.Infinite);

            ts_interval = 1000;
            TS_Timer = new Timer(TS_Tick, null, ts_interval, Timeout.Infinite);

            fix_interval = new TimeSpan(24, 0, 0);
            Fix_Timer = new Timer(Fix_Tick, null, fix_interval, fix_interval);            
        }

        private void MM_Tick(object data)
        {
            if (Flags.backup_restore_in_proc) return; //проверка на резервирование или восстановление снэпшота

            //ставим в очередь
            Queues.prdf_queue.Enqueue(() => { Sys.core.ManageMargin(); });

            MM_Timer.Change(mm_interval, Timeout.Infinite);
        }

        private void SL_Tick(object data)
        {
            if (Flags.backup_restore_in_proc) return; //проверка на резервирование или восстановление снэпшота

            //ставим в очередь
            Queues.prdf_queue.Enqueue(() => { Sys.core.ManageSLs(); });

            SL_Timer.Change(sl_interval, Timeout.Infinite);
        }

        private void TP_Tick(object data)
        {
            if (Flags.backup_restore_in_proc) return; //проверка на резервирование или восстановление снэпшота

            //ставим в очередь
            Queues.prdf_queue.Enqueue(() => { Sys.core.ManageTPs(); });

            TP_Timer.Change(tp_interval, Timeout.Infinite);
        }

        private void TS_Tick(object data)
        {
            if (Flags.backup_restore_in_proc) return; //проверка на резервирование или восстановление снэпшота

            //ставим в очередь
            Queues.prdf_queue.Enqueue(() => { Sys.core.ManageTSs(); });

            TS_Timer.Change(ts_interval, Timeout.Infinite);
        }

        private void Fix_Tick(object data)
        {
            Queues.prdf_queue.Enqueue(() =>
            {
                Snapshot.BackupMasterSnapshot(); //блокирующий снэпшот Master-ядра
                Sys.fixman.RestartApp(Sys.core.GetSenderCompIds()); //перезапуск FIX с новыми аккаунтами
            });

            Console.WriteLine("[24h basis] Backup Master snapshot & FIX restart queued");
        }
    }
}
