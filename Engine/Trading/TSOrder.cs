﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore
{
    [Serializable]
    class TSOrder : Order
    {
        internal decimal Offset { get; private set; }

        internal TSOrder() : base() //конструктор по умолчанию
        {
            Offset = 0m;
        }

        internal TSOrder(int user_id, decimal original_amount, decimal actual_amount, decimal rate, decimal offset) : base(user_id, original_amount, actual_amount, rate) //конструктор трейлинг-стопа
        {
            Offset = offset;
        }
    }
}
