﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class TradeMsg : IJsonSerializable
    {
        private Trade trade;

        internal TradeMsg(Trade trade) //конструктор сообщения
        {
            this.trade = trade;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewTrade, trade);
        }
    }
}
